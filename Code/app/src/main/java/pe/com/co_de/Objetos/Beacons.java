package pe.com.co_de.Objetos;

public class Beacons {
    String id, titulo, descripcion, distancia;

    public Beacons() {
    }

    public Beacons(String id, String titulo, String descripcion, String distancia) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.distancia = distancia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

}
