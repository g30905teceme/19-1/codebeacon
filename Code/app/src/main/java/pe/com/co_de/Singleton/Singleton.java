package pe.com.co_de.Singleton;

import android.content.Context;

/**
 * Created by Francisco on 12/11/2017.
 */

public class Singleton {

    private static Singleton instance = null;
    public String token = "";

    private Singleton() {}

    public static synchronized Singleton getInstance() {
        if(instance == null)
            instance = new Singleton();
        return instance;
    }

    public void init(Context context) {}

    public void clear() {
        token = "";
    }
}
