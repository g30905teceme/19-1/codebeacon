package pe.com.co_de.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;

import pe.com.co_de.MainActivity;
import pe.com.co_de.R;
import pe.com.co_de.SQLite.AdminSQLite;
import pe.com.co_de.Singleton.Singleton;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText user, pass;
    Button agregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user = findViewById(R.id.usuario);
        pass = findViewById(R.id.contrasena);
        agregar = findViewById(R.id.agregar);

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdminSQLite admin = new AdminSQLite(v.getContext(), "datos", null, 1);
                SQLiteDatabase db = admin.getWritableDatabase();
                String[] col = new String[]{"user", "pass", "token"};
                Cursor fi = db.query("usuarios", col, null, null, null, null, null);
                if (fi != null) {
                    ContentValues registro = new ContentValues();
                    if (!fi.moveToNext()) {
                        registro.put("user", user.getText().toString());
                        registro.put("pass", pass.getText().toString());
                        registro.put("token", "token");
                        db.insert("usuarios", null, registro);
                    }
                }
                db.close();
                Singleton.getInstance().token = "token";
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                LoginActivity.this.finish();
            }
        });
    }
}
