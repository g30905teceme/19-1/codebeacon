var express = require('express')
var app = express()
var http = require('http').Server(app)
var cookieParser = require('cookie-parser')
var parser = require('body-parser')
var jwt = require('jsonwebtoken')
var moment = require('moment')
var mysql = require('mysql')
var fs = require('fs')
var CronJob = require('cron').CronJob
var config = require('./config.js')

app.set('views', __dirname + '/View')
app.set('view engine', 'ejs')
app.use(express.static(__dirname + '/Public'))
app.set('key', config.secret)
app.use(parser())
app.use(cookieParser())

var con = mysql.createConnection({
  host: config.host,
  user: config.user,
  password: config.password,
  database: config.database
});

con.connect(function(error) {
  if (error) throw error;
});

//API REST
app.post('/login', function(req, res) {
  var user = req.body.user
  var pass = req.body.pass
  con.query("SELECT id FROM usuarios WHERE user = '" + user + "' AND pass = '" + pass + "';", function (error, result) {
    if(error){
      res.json({ msg: 'error', data: error })
    }else if(result.length > 0) {
      var m = moment().hour(0).minute(0).second(0).millisecond(0)
      m = m.add(1, 'days')
      var ms = moment(m).diff(moment())
      var r = moment.duration(ms)
      var restante = Math.floor(r / 1000)
      res.json({ msg: 'ok', data: jwt.sign({user: result[0].id.toString()}, config.secret, { expiresIn: restante }) })
    }else{
      res.json({ msg: 'error', data: [] })
    }
  })
})

app.get('/beacon/:uuid', function(req, res) {
  var uuid = req.params.uuid
  con.query("SELECT * FROM ibeacons WHERE uuid = '" + uuid + "';", function (error, result) {
    if(error){
      res.json({ msg: 'error', data: error })
    }else if(result.length > 0) {
      var json = {
        nombre: result[0].nombre,
        descripcion: result[0].descripcion
      }
      res.json({ msg: 'ok', data: json })
    }else{
      res.json({ msg: 'error', data: [] })
    }
  })
})

app.get('/editar/:uuid', function(req, res) {
  var uuid = req.params.uuid
  var data = req.body
  /*
  con.query("SELECT * FROM ibeacons WHERE uuid = '" + uuid + "';", function (error, result) {
    if(error){
      res.json({ msg: 'error', data: error })
    }else if(result.length > 0) {
      var json = {
        nombre: result[0].nombre,
        descripcion: result[0].descripcion
      }
      res.json({ msg: 'ok', data: json })
    }else{
      res.json({ msg: 'error', data: [] })
    }
  })*/
})

app.get('/eliminar/:uuid', function(req, res) {
  var uuid = req.params.uuid
  con.query("DELETE FROM ibeacons WHERE uuid = '" + uuid + "';", function (error, result) {
    if(error){
      res.json({ msg: 'error', data: error })
    }else{
      res.redirect('/')
    }
  })
})

app.get('/', function(req, res) {
  res.render('index')
})

app.get('/crear', function(req, res) {
  res.render('crear')
})

/*app.get('/*', function(req, res) {
  res.redirect("https://co-de.com.pe/")
})

app.post('/*', function(req, res) {
  res.redirect("https://co-de.com.pe/")
})*/

//SocketIO
/*io.on('connection', function(socket) {
  socket.on('get', function(data) {
    socket.emit('get', {data: []})
  })
})*/

//Tareas
/*new CronJob('00 * * * * *', function() {
  //actualizar()
}, null, true, 'America/Lima')*/

//Server
http.listen(config.port, function() {
  console.log('Servidor montado *:' + config.port)
})
